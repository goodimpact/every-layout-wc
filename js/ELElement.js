/**
 * @module ELElement
 * @description
 * Base class for all Every Layout custom elements
 * All Components should inherit this class
 */
export default class ELElement extends HTMLElement {
	constructor() {
		super();
		this.classList.add("wc");
	}
}
